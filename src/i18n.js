import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// the translations
// (tip move them in a JSON file and import them)
const resources = {
    en: {
        translation: {
            "home":"Home",
            "about":"About",
            "new-place": "Add a new place",
            "know_a_place": "Know a place?",
            "product-name": "velobaza",
            "menu": "Menu",
            "made_by_nice_people": "Made by nice people",
            "search": "Search",
            "stores": "Stores",
            "bikes": "Bikes",
            "select_category": "Select category",
            "head_know_where_to_look": "Hey, welcome!",
            "know_where_to_look": "You just need to know where to look.",
            "select_zone": "Select zone",
            "select_a_photo": "Select a photo",
            "center": "Center",
            "botanica": "Botanika",
            "rascani": "Riscani",
            "buiucani": "Buiucani",
            "ciocana": "Ciocana",
            "submit": "Submit",
            "add": "Add",
            "photo_file_input": "Photo input"
        }
    },
    ro: {
        translation: {
            "home":"Acasa",
            "about":"Despre",
            "new-place": "Adauga un loc",
            "know_a_place": "Stii un point?",
            "product-name": "velobaza",
            "menu": "Meniul",
            "made_by_nice_people": "Creat de oameni faini",
            "search": "Cautare",
            "select_category": "Selectarea categoriei",
            "stores": "Magazine",
            "bikes": "Biciclete",
            "close": "Inchide",
            "head_know_where_to_look": "Hei, bine ai venit!",
            "know_where_to_look": "Trebuie doar să știi unde să cauți.",
            "select_zone": "Selecteaza zona",
            "select_a_photo": "Selecteaza o poza",
            "center": "Center",
            "botanica": "Botanica",
            "rascani": "Rascani",
            "buiucani": "Buiucani",
            "ciocana": "Ciocana",
            "submit": "Trimite",
            "add": "Adauga",
            "photo_file_input": "Poza"
        }
    },
    ru: {
        translation: {
            "home":"Главная",
            "about":"О нас",
            "new-place": "Добавить место",
            "know_a_place": "Знаеш место?",
            "product-name": "velobaza",
            "menu": "Меню",
            "made_by_nice_people": "Создано четкими пацанами и чиками",
            "search": "Поиск",
            "stores": "Магазины",
            "bikes": "Велосипеды",
            "select_category": "Выбор категории",
            "head_know_where_to_look": "Привет, добро пожаловать!",
            "know_where_to_look": "Тебе просто нужно знать, где искать",
            "select_zone": "Выбери зону",
            "select_a_photo": "Выбрать фото",
            "center": "Центр",
            "botanica": "Ботаника",
            "rascani": "Рышкановка",
            "buiucani": "Буюканы",
            "ciocana": "Чеканы",
            "submit": "Отправить",
            "add": "Добавить",
            "photo_file_input": "Фото"
        }
    }
};

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        lng: "ru",

        keySeparator: false, // we do not use keys in form messages.welcome

        interpolation: {
            escapeValue: false // react already safes from xss
        }
    });

export default i18n;
