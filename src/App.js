import React, {useState} from 'react';
import {Link, Route, Switch, BrowserRouter, useHistory} from "react-router-dom";
import {useTranslation} from "react-i18next";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form"
import FormControl from "react-bootstrap/FormControl"
import Button from "react-bootstrap/Button"
import Modal from "react-bootstrap/Modal";
import TreeViewMenu from 'react-simple-tree-menu'

import About from "./pages/About";
import NewPlace from "./pages/NewPlace";
import Home from "./pages/Home";
import Places from "./pages/Places";
import Category from "./components/Category";

import './App.scss';


function App() {
    const {t} = useTranslation();
    require('dotenv').config()
    const history = useHistory();
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const dataCategories = normalizeDataCategories();

    function normalizeDataCategories() {
        let data = [];
        for (let category of Category.getList()) {
            let children = [];
            for (let categoryChild of category.children) {
                let dataItem = {
                    "key": categoryChild.id.concat("_" + category.id),
                    "label": t(categoryChild.code) ? t(categoryChild.code) : categoryChild.defaultName,
                    "code" : categoryChild.code
                }
                children.push(dataItem)
            }
            let dataItem = {
                "key": category.id,
                "label": t(category.code) ? t(category.code) : category.defaultName,
                "code": category.code,
                "nodes": children
            };

            data.push(dataItem)
        }
        return data;
    }

    function goToAnywhereByURL(url) {
        history.push(url)
    }

    return (
        <Container>
          <BrowserRouter>
              <Navbar bg="light">
                  <Navbar.Brand href="/">
                      <img
                          alt=""
                          src="logo_uncolored.svg"
                          width="35"
                          height="35"
                          className="d-inline-block align-top"
                      />{' '}
                      {t('product-name')}
                  </Navbar.Brand>
                  <Nav className="justify-content-end">
                      <Nav.Item>
                          <Nav.Link href="/new-place">{t('new-place')}</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                          <Nav.Link onClick={handleShow}>{t('menu')}</Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                          <Nav.Link href='/about'>{t('about')}</Nav.Link>
                      </Nav.Item>
                  </Nav>
                  <Form inline>
                      <FormControl type="text" placeholder={t('search')} className="mr-sm-2" />
                      <Button variant="outline-success">{t('search')}</Button>
                  </Form>
              </Navbar>

              <Modal show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                      <Modal.Title>{t('select_category')}</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                      <TreeViewMenu data={dataCategories}
                                    onClickItem={props => goToAnywhereByURL("/?" + props.code)}
                                    debounceTime={125}>
                           {/*todo find solution and render items with stiles https://github.com/iannbing/react-simple-tree-menu*/}
                          {/*{({ search, items }) => (*/}
                          {/*    <>*/}
                          {/*        <FormControl type="text" onChange={e => search(e.target.value)} placeholder={t('search')} />*/}
                          {/*        <ListGroup>*/}
                          {/*            {items.map(props => (*/}
                          {/*                // You might need to wrap the third-party component to consume the props*/}
                          {/*                // check the story as an example*/}
                          {/*                // https://github.com/iannbing/react-simple-tree-menu/blob/master/stories/index.stories.js*/}
                          {/*                <ListGroup.Item action href={"/"+props.code}>{props.label}</ListGroup.Item>*/}
                          {/*            ))}*/}
                          {/*        </ListGroup>*/}
                          {/*    </>*/}
                          {/*)}*/}
                      </TreeViewMenu>
                  </Modal.Body>
                  <Modal.Footer>
                      <Button variant="secondary" onClick={handleClose}>
                          {t('close')}
                      </Button>
                  </Modal.Footer>
              </Modal>
              <Switch>
                  <Route exact path="/new-place" component={NewPlace} />
                  <Route exact path="/places" component={Places} />
                  <Route exact path="/about" component={About} />
                  <Route exact path="/" component={Home} />
              </Switch>
          </BrowserRouter>
        </Container>
  );
}

export default App;
