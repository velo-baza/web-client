import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Map from "../components/Map";
import {useTranslation} from "react-i18next";


function NewPlace() {
    const {t} = useTranslation();

    return (
        <Row>
            <Col sm={8}>
                <Map />
                <Form>
                    <Form.File
                        id="file"
                        label={t("photo_file_input")}
                        data-browse={t('select_a_photo')}
                        custom
                    />
                </Form>
            </Col>
            <Col sm={4}>
                <Form>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="name@example.com" />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlSelect2">
                        <Form.Label>{t('select_category')}</Form.Label>
                        <Form.Control as="select" multiple>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </Form.Control>
                    </Form.Group>
                </Form>
            </Col>
        </Row>
    );
}

export default NewPlace;
