import React from 'react';
import Map from "../components/Map";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Alert from "react-bootstrap/Alert";
import {useTranslation} from "react-i18next";
import Button from "react-bootstrap/Button";


function Home() {
    const {t} = useTranslation();

    return (
        <Row>
            <Col sm={8}>
                <Map />
            </Col>
            <Col sm={4}>
                <Alert variant="info">
                    <Alert.Heading>{t('head_know_where_to_look')}</Alert.Heading>
                    <p>
                        {t('know_where_to_look')}
                    </p>
                </Alert>
                <DropdownButton id="dropdown-basic-button" title={t('select_zone')}>
                    <Dropdown.Item>{t('center')}</Dropdown.Item>
                    <Dropdown.Item>{t('botanica')}</Dropdown.Item>
                    <Dropdown.Item>{t('rascani')}</Dropdown.Item>
                    <Dropdown.Item>{t('buiucani')}</Dropdown.Item>
                    <Dropdown.Item>{t('ciocana')}</Dropdown.Item>
                </DropdownButton>

                <Alert variant="success">
                    <Alert.Heading>{t('new-place')}</Alert.Heading>
                    <hr />
                    <div className="d-flex justify-content-end">
                        <Button href={'/new-place'} variant="outline-success">
                            {t('add')}
                        </Button>
                    </div>
                </Alert>
            </Col>
        </Row>

    );
}

export default Home;
