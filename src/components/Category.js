import React, {Component} from "react";


class Category extends Component {

    static getList() {
        return [
            {
                "id": "1",
                "defaultName" : "Stores",
                "code": "stores",
                "parentId": 34,
                "children" : [
                    {
                        "id": "4",
                        "defaultName" : "Bikes",
                        "code": "bikes",
                        "imageUrl": "",
                        "parentId": 1
                    },
                ]
        }
        ]
    }

    render() {
        return (
            <div>
                {/* list of categories */}
            </div>
        );
    }
}

export default Category;
