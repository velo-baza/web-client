import React, {Component} from "react";
import ReactMapGL, {GeolocateControl, NavigationControl} from "react-map-gl"


const geolocateStyle = {
    position: 'absolute',
    top: 0,
    left: 0,
    margin: 10
};


class Map extends Component {
    state = {
        viewport: {
            width: 750,
            height: 750,
            latitude: 47.003670,
            longitude: 28.907089,
            zoom: 10
        }
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ReactMapGL
                {...this.state.viewport}
                onViewportChange={(viewport) => this.setState({viewport})}
                mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_ACCESS_TOKEN}>
                <GeolocateControl
                    style={geolocateStyle}
                    positionOptions={{enableHighAccuracy: true}}
                    trackUserLocation={true}
                />
                <NavigationControl />
            </ReactMapGL>
        );
    }
}

export default Map;
